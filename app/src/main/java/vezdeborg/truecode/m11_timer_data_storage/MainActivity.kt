package vezdeborg.truecode.m11_timer_data_storage

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import vezdeborg.truecode.m11_timer_data_storage.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var prefs: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val editText = binding.editText
        val textView = binding.textView

        prefs = getSharedPreferences("Prefs", MODE_PRIVATE)
        val repository = Repository(prefs)

        textView.text = repository.getText()

        binding.saveButton.setOnClickListener {
            repository.saveText(editText.text.toString())
            Toast.makeText(this, "${editText.text.toString()} сохранено в SP", Toast.LENGTH_SHORT).show()
            textView.text = repository.getDataFromLocalVariable()
        }

        binding.clearButton.setOnClickListener {
            repository.clearText()
            editText.text?.clear()
            textView.text = ""
        }
    }
}