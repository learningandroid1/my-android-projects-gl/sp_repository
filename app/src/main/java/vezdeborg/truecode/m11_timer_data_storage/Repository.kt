package vezdeborg.truecode.m11_timer_data_storage

import android.content.SharedPreferences
import android.widget.Toast

class Repository(private val preferences: SharedPreferences) {
    private val editor = preferences.edit()
    private var localVariable: String? = null

    fun getDataFromSharedPreferences(): String? = preferences.getString(TEXT, "Default")

    fun getDataFromLocalVariable(): String? = localVariable

    fun saveText(text: String) {
        localVariable = text
        editor.putString(TEXT, text).apply()
        //Toast.makeText(this, "$text сохранено в SP", Toast.LENGTH_SHORT).show() какой контекст прописать?
    }

    fun getText(): String? {
        return getDataFromLocalVariable() ?: getDataFromSharedPreferences()
    }

    fun clearText() {
        localVariable = null
        editor.clear()
    }

    companion object {
        private const val TEXT = "Text"
    }
}